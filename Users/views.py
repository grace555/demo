from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Image, ImageVote, Tags, ImageTags
from django.contrib.auth.decorators import login_required
from django.http import Http404
import pyuploadcare.api_resources as upload_api
from pyuploadcare import InvalidRequestError
from django_comments.models import Comment
from django import http
from secretballot.models import Vote
from django.db.models import Q
from django.contrib.auth.models import User
import os.path
import time
import Itransition.settings as settings


def find(request):
    string = request.POST.get('string')
    backup = string if string else ""
    string = string.lower() if string else ""
    users = []
    images = []
    for user in User.objects.all():
        if user.username.lower().find(string) != -1 or user.first_name.lower().find(string) != -1 or user.last_name.lower().find(string) != -1:
            users.append(user)
    for image in Image.objects.all():
        if image.top_text.lower().find(string) != -1 or image.bottom_text.lower().find(string) != -1 or image.tags.lower().find(string) != -1:
            images.append(image)
    context = {'string': backup, 'users': users, 'images': images}
    return render(request, 'search.html', context)


def find_by_tag(request, tag):
    tag = Tags.objects.filter(tag=tag).first()
    image_tags = ImageTags.objects.filter(tag=tag)
    amount = len(image_tags)
    context = {'image_tags': image_tags[::-1], 'tag': tag, 'amount': amount}
    return render(request, "tag.html", context)


def user_page(request, id):
    user = get_object_or_404(User, id=id)
    images = Image.objects.filter(author=user.username)
    name = " ".join([user.first_name, user.last_name])
    comments = Comment.objects.filter(Q(user_name=name) | Q(user_name=user.username))
    total_likes = 0
    total_vote = 0
    for x in range(len(comments)):
        total_likes += comments[x].total_upvotes
    count = 0
    for x in range(len(images)):
        add = Image.get_likes(images[x])
        total_vote += add
        if add != 0:
            count += 1
    if count != 0:
        total_vote /= count
    context = {'images': images, 'username': user.username, "likes": total_likes,
               'vote': total_vote.__round__(2), 'amount': len(images)}
    return render(request, "user.html", context)


@login_required()
def like_comment(request, id):
    comment = get_object_or_404(Comment, id=id)
    try:
        if Vote.objects.get(object_id=id, token=request.user.id):
            comment.remove_vote(request.user.id)
    except Vote.DoesNotExist:
        comment.add_vote(request.user.id, 1)
    return http.HttpResponseRedirect(comment.content_object.get_absolute_url())


def delete_comment(request, id):
    comment = get_object_or_404(Comment, id=id)
    if comment.user.id != request.user.id and not request.user.is_staff:
        raise Http404
    comment.delete()
    return http.HttpResponseRedirect(comment.content_object.get_absolute_url())


@login_required()
def vote_image(request, slug, x):
    vote = ImageVote.objects.get_or_create(user_id=request.user.id, image_slug=slug)
    vote[0].vote_value = int(x)
    vote[0].save()
    return http.HttpResponseRedirect('/open/' + slug + '/')


class UploadView(CreateView):
    model = Image
    fields = ('slug', 'image', 'result', 'top_text', 'bottom_text', 'author', 'tags')


class ChangeView(UpdateView):
    model = Image
    fields = ('image', 'result', 'top_text', 'bottom_text', 'author', 'tags')

    def get_object(self):
        obj = super(UpdateView, self).get_object()
        obj.result = None

        if self.request.user.username != obj.author and not self.request.user.is_staff:
            raise PermissionError()

        try:
            if obj.image.is_removed():
                raise ValueError('File was deleted.')
        except (InvalidRequestError, ValueError):
            new_image = upload_api.File.upload(open('Users/static/' + obj.normal_url(), 'rb'))
            new_image.store()
            obj.result = new_image
            new_source = upload_api.File.upload(open('Users/static/' + obj.source_url(), 'rb'))
            new_source.store()
            obj.image = new_source
            obj.save()
        return obj


class RemoveView(DeleteView):
    model = Image
    success_url = '/'

    def get_object(self):
        obj = super(DeleteView, self).get_object()
        if self.request.user.username != obj.author and not self.request.user.is_staff:
            raise PermissionError()
        return obj

    def post(self, request, *args, **kwargs):
        obj = super(DeleteView, self).get_object()
        os.remove(settings.BASE_DIR + "\\" + os.path.relpath('Users/static/' + obj.normal_url()))
        os.remove(settings.BASE_DIR + "\\" + os.path.relpath('Users/static/' + obj.source_url()))
        os.remove(settings.BASE_DIR + "\\" + os.path.relpath('Users/static/' + obj.mini_url()))
        ImageTags.objects.filter(image_id=obj.slug).delete()
        ImageVote.objects.filter(image_slug=obj.slug).delete()
        Comment.objects.filter(object_pk=obj.slug).delete()
        return DeleteView.post(self, request, args, kwargs)


class ImageView(DetailView):
    model = Image

    def get_object(self):
        obj = super(ImageView, self).get_object()
        obj.tags = ImageTags.objects.filter(image=obj)
        return obj

    def get_context_data(self, **kwargs):
        context = super(ImageView, self).get_context_data(**kwargs)
        obj = super(ImageView, self).get_object()
        context['author_id'] = User.objects.filter(username=obj.author).first().id if len(User.objects.all()) else 0
        context['stars'] = self.model.get_likes(obj).__round__(2)
        round_likes = int(self.model.get_likes(obj))
        context['filled_star_amount'] = round_likes
        context['star_amount'] = range(5)
        context['userId'] = self.request.user.id
        image_votes = ImageVote.objects.all().filter(image_slug=obj.slug, user_id=self.request.user.id)

        comments = Comment.objects.filter(object_pk=obj.slug, is_removed=False)
        user_likes = []
        for i in range(len(comments)):
            user_likes.append(True)
            try:
                im = Vote.objects.get(token=self.request.user.id, object_id=comments[i].id)
            except Vote.DoesNotExist:
                user_likes.pop()
                user_likes.append(False)
        data = zip(comments, user_likes)
        context['data'] = data
        context['can_vote'] = (len(image_votes) == 0)
        path = settings.BASE_DIR + "\\" + os.path.relpath('Users/static/' + obj.normal_url())
        context['date'] = time.strftime("%d.%m.%y, %H:%M:%S", time.gmtime(os.path.getmtime(path)))
        return context


def home(request):
    def get_tag_cloud():
        _cloud = {}
        for tag in Tags.objects.all():
            _cloud[tag.tag] = len(ImageTags.objects.filter(tag=tag))
        _cloud = list(_cloud.items())
        _cloud.sort(key=lambda item: item[1], reverse=True)
        return _cloud[:10]

    if 'theme' not in request.session:
        request.session['theme'] = 'default'
        request.session.modified = True

    images = Image.objects.all()[::-1]
    cloud = get_tag_cloud()
    context = {'images': images, 'cloud': cloud}
    return render(request, "profile.html", context)


@login_required()
def change_theme(request):
    if request.session['theme'] == 'dark':
        request.session['theme'] = 'default'
        request.session.modified = True
    else:
        request.session['theme'] = 'dark'
        request.session.modified = True
    return redirect(request.POST.get('next'))
