# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Users', '0003_image_result'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='author',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
