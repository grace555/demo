# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Users', '0005_auto_20151108_1630'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imagevote',
            name='VoteValue',
            field=models.IntegerField(null=True),
        ),
    ]
