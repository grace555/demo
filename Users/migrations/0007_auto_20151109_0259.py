# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Users', '0006_auto_20151108_1825'),
    ]

    operations = [
        migrations.RenameField(
            model_name='imagevote',
            old_name='Id',
            new_name='id',
        ),
        migrations.RenameField(
            model_name='imagevote',
            old_name='ImageSlug',
            new_name='image_slug',
        ),
        migrations.RenameField(
            model_name='imagevote',
            old_name='UserId',
            new_name='user_id',
        ),
        migrations.RenameField(
            model_name='imagevote',
            old_name='VoteValue',
            new_name='vote_value',
        ),
    ]
