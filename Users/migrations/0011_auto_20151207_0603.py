# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Users', '0010_image_source_path'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='image',
            name='image_path',
        ),
        migrations.RemoveField(
            model_name='image',
            name='source_path',
        ),
    ]
