# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Users', '0008_auto_20151110_0148'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='image_path',
            field=models.CharField(null=True, blank=True, max_length=50),
        ),
    ]
