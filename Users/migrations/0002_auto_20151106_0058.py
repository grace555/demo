# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Users', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='image',
            old_name='bottomText',
            new_name='bottom_text',
        ),
        migrations.RenameField(
            model_name='image',
            old_name='topText',
            new_name='top_text',
        ),
    ]
