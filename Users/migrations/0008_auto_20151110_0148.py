# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Users', '0007_auto_20151109_0259'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImageTags',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='Tags',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('tag', models.CharField(max_length=30)),
            ],
        ),
        migrations.AddField(
            model_name='image',
            name='tags',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='imagetags',
            name='image',
            field=models.ForeignKey(to='Users.Image'),
        ),
        migrations.AddField(
            model_name='imagetags',
            name='tag',
            field=models.ForeignKey(to='Users.Tags'),
        ),
    ]
