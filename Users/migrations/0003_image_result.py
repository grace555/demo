# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import pyuploadcare.dj.models


class Migration(migrations.Migration):

    dependencies = [
        ('Users', '0002_auto_20151106_0058'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='result',
            field=pyuploadcare.dj.models.ImageField(null=True),
        ),
    ]
