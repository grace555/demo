# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import pyuploadcare.dj.models


class Migration(migrations.Migration):

    dependencies = [
        ('Users', '0004_image_author'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImageVote',
            fields=[
                ('Id', models.AutoField(primary_key=True, serialize=False)),
                ('ImageSlug', models.SlugField(max_length=10)),
                ('UserId', models.IntegerField()),
                ('VoteValue', models.IntegerField()),
            ],
        ),
        migrations.AlterField(
            model_name='image',
            name='result',
            field=pyuploadcare.dj.models.ImageField(blank=True, null=True),
        ),
    ]
