# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Users', '0009_image_image_path'),
    ]

    operations = [
        migrations.AddField(
            model_name='image',
            name='source_path',
            field=models.CharField(blank=True, null=True, max_length=50),
        ),
    ]
