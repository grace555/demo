import string
import random
from django.db import models
from pyuploadcare.dj import ImageField
import pyuploadcare.api_resources as UploadAPI
from django.contrib.auth.models import User
import PIL.Image as PImage
from PIL import ImageDraw2
import urllib.request
import io
import os
from django_comments.models import Comment
import secretballot
import Itransition.settings as settings

def create_text(draw, image, inner_size, is_top):
    if is_top:
        text = image.top_text
        add = 0
    else:
        text = image.bottom_text
        add = inner_size + 150
    size = 64
    font = ImageDraw2.Font('white', os.path.join('C:\Windows\Fonts', 'arial.ttf'), size)
    while draw.textsize(text, font)[0] > 500:
        font = ImageDraw2.Font('white', os.path.join('C:\Windows\Fonts', 'arial.ttf'), size)
        size -= 1
    size = draw.textsize(text, font)
    draw.text((300 - size[0]/2, add + 75 - size[1]/2), text, font)

def get_full_path(path):
    return settings.BASE_DIR + "\\" + os.path.relpath(path)

def create_demotivator(image):
    url = str(image.image)
    url += '-/stretch/off/-/resize/500x/'
    path = settings.BASE_DIR + "\\" + os.path.relpath('Users/static/images/demotivators/' + str(image.slug) + '-source.jpg')
    urllib.request.urlretrieve(url, path)
    path = io.BytesIO(urllib.request.urlopen(url).read())
    im = PImage.open(path)
    if im.size[0] < 500:
        im = im.resize((500, int(im.size[1] * (500 / im.size[0]))))
    height = im.size[1] + 300
    dem = PImage.new('RGB', (600, height))
    draw = ImageDraw2.Draw(dem)

    create_text(draw, image, im.size[1], True)
    create_text(draw, image, im.size[1], False)

    draw.rectangle((40, 140, 560, im.size[1] + 160), 'white')
    dem.paste(im, (50, 150))
    path = settings.BASE_DIR + "\\" + os.path.relpath('Users/static/images/demotivators/' + str(image.slug) + '.jpg')
    dem.save(path, 'jpeg')
    new = UploadAPI.File.upload(open(get_full_path('Users/static/images/demotivators/' + str(image.slug) + '.jpg'), 'rb'))
    new.store()
    urllib.request.urlretrieve(str(new) + '-/preview/1000x200/-/quality/best/', get_full_path('Users/static/images/demotivators/' + str(image.slug) + '-mini.jpg'))
    return new


class Image(models.Model):
    top_text = models.CharField(max_length=50)
    bottom_text = models.CharField(max_length=50)
    author = models.CharField(max_length=50, null=True)
    slug = models.SlugField(max_length=10, primary_key=True, blank=True)
    image = ImageField(manual_crop="")
    result = ImageField(null=True, blank=True)
    tags = models.CharField(max_length=50, null=True, blank=True)

    def __repr__(self):
        return u'<Image slug={0} image={1}>'.format(self.slug, self.image)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = ''.join(random.sample(string.ascii_lowercase, 6))
        if not self.result:
            self.result = create_demotivator(self)

        ImageTags.objects.filter(image=self).delete()
        for tag in self.tags.split():
            new_tag = Tags.objects.get_or_create(tag=tag)[0]
            ImageTags.objects.create(image=self, tag=new_tag)

        super(Image, self).save(*args, **kwargs)

    def get_likes(self):
        slug = self.slug
        image_vote = ImageVote.objects.filter(image_slug=slug)
        total = 0
        if image_vote:
            if image_vote[0].vote_value:
                for x in range(len(image_vote)):
                    total += image_vote[x].vote_value
                if len(image_vote) != 0:
                    total /= len(image_vote)
        return total

    def normal_url(self):
        return 'images/demotivators/' + self.slug + '.jpg'

    def source_url(self):
        return 'images/demotivators/' + self.slug + '-source.jpg'

    def mini_url(self):
        return 'images/demotivators/' + self.slug + '-mini.jpg'

    @models.permalink
    def get_absolute_url(self):
        return 'detail', (), {'pk': self.pk}


class ImageVote(models.Model):
    id = models.AutoField(primary_key=True)
    image_slug = models.SlugField(max_length=10)
    user_id = models.IntegerField()
    vote_value = models.IntegerField(null=True)


class Tags(models.Model):
    id = models.AutoField(primary_key=True)
    tag = models.CharField(max_length=30)


class ImageTags(models.Model):
    image = models.ForeignKey('Image')
    tag = models.ForeignKey('Tags')

secretballot.enable_voting_on(Comment)
