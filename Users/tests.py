from django.test import TestCase, Client
from Users.models import Image
import pyuploadcare.api_resources as upload_api
import os


# Create your tests here.
class PagesTests(TestCase):
    def test_homepage(self):
        client = Client()
        response = client.get('/')
        self.assertEqual(response.status_code, 200)


class DemotivatorsTests(TestCase):
    client = Client()

    def setUp(self):
        image = upload_api.File.upload(open('Users/static/images/demotivators/_test.png', 'rb'))
        self.client.post('/new/', {
            'top_text': 'Test',
            'bottom_text': 'Test',
            'author': 'Angel',
            'image': image,
            'tags': 'TestTag',
        })

    def test_create_page(self):
        response = self.client.get('/new/')
        self.assertEqual(response.status_code, 200)

    def test_homepage(self):
        response = self.client.get('/')
        self.assertEqual(len(response.context['images']), 1)

    def test_tags(self):
        response = self.client.get('/')
        self.assertIn('TestTag', str(response.content))

    def test_detail_page(self):
        obj = Image.objects.first()
        response = self.client.get('/open/' + obj.slug + '/')
        self.assertIn(obj.author, str(response.content))

    def test_theme(self):
        response = self.client.post('/change_theme/', {
            'next': '/'
        })
        self.assertEqual(response.status_code, 302)

    def test_search(self):
        response = self.client.post('/search/', {
            'string': 'MyTest',
        })
        self.assertEqual(len(response.context['images']), 0)
        response = self.client.post('/search/', {
            'string': 'Test',
        })
        self.assertEqual(len(response.context['images']), 1)

    def doCleanups(self):
        obj = Image.objects.first()
        os.remove('Users/static/' + obj.normal_url())
        os.remove('Users/static/' + obj.source_url())
        os.remove('Users/static/' + obj.mini_url())
