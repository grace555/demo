from django.contrib import admin
from .models import Image, ImageVote, Tags, ImageTags

# Register your models here.
admin.site.register(Image)
admin.site.register(ImageVote)
admin.site.register(Tags)
admin.site.register(ImageTags)
