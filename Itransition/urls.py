"""Itransition URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls import include, url
from django.contrib import admin
from Users import views

urlpatterns = (
    url(r'^$', 'Users.views.home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^accounts/profile', 'Users.views.home', name='home'),
    url(r'^new/', views.UploadView.as_view(), name='new'),
    url(r'^open/(?P<pk>[a-z]{6})/$', views.ImageView.as_view(), name='detail'),
    url(r'^change_theme/$', 'Users.views.change_theme', name='change_theme'),
    url(r'^$/change/', 'Users.views.home', name='change'),
    url(r'^edit/(?P<pk>[a-z]{6})/$', views.ChangeView.as_view(), name='edit'),
    url(r'^delete/(?P<pk>[a-z]{6})/$', views.RemoveView.as_view(), name='delete'),
    url(r'^comments/', include('django_comments.urls')),
    url(r'^comments/like/(?P<slug>.*)/(?P<x>[0-9]+)/$', views.vote_image, name='like'),
    url(r'^comments/like/(?P<slug>.*)/$', views.vote_image, name='like'),
    url(r'^comments/vote/(?P<id>.*)/$', views.like_comment, name='vote'),
    url(r'^comments/delete_own/(?P<id>.*)/$', views.delete_comment, name='delete_comment'),
    url(r'^user/(?P<id>.*)/$', views.user_page, name='user'),
    url(r'^tag/(?P<tag>.*)/$', views.find_by_tag, name='tag'),
    url(r'^search/$', views.find, name='search'),
)
